"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.connect = exports.Provider = exports.useMiddleware = exports.useReducer = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _react = _interopRequireWildcard(require("react"));

var _connect = _interopRequireDefault(require("react-redux/es/connect/connect"));

var _Provider = _interopRequireDefault(require("react-redux/es/components/Provider"));

var _redux = require("redux");

var _reduxSaga = _interopRequireDefault(require("redux-saga"));

var _effects = require("redux-saga/effects");

var helpers = _interopRequireWildcard(require("./helpers"));

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return (0, _typeof2["default"])(key) === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if ((0, _typeof2["default"])(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if ((0, _typeof2["default"])(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/**
 * An object that is used to build the initial state tree for the
 * entire app. Each call to `connect()` will add a new key to this
 * object.
 * @type {Object}
 */
var combinedInitialState = {};
/* =================================== */

/* STORE
/* =================================== */

/**
 * Creates the saga middleware function.
 * @type {Function}
 */

var sagaMiddleware = (0, _reduxSaga["default"])();
/**
 * Creates the saga store enhancer.
 * @type {Function}
 */

var sagaEnhancer = (0, _redux.applyMiddleware)(sagaMiddleware);
/**
 * Creates a middleware function that is used to enable Redux devTools.
 * in the browser.
 * @type {Function}
 */

var devTools = (0, _redux.compose)(window.devToolsExtension ? window.devToolsExtension() : function (foo) {
  return foo;
});
/**
 * This is not the actual store object. This is a wrapper object
 * that manages the Redux store instance. Use `store.getInstance()`
 * to get a reference to the Redux store.
 */

var store = {
  /**
   * An object that is used as a map to store references to registered
   * reducers. This object is used by `getRootReducer()` to create the
   * root reducer for the store.
   * @type {Object}
   */
  reducers: {},
  sagas: [],

  /**
   * An array of middlewares to use when creating the store.
   * Use exported method `useMiddleware()` to add other middleware
   * functions to this list.
   * @type {Array}
   */
  middlewares: [sagaEnhancer, devTools],

  /**
   * Creates a new Redux store instance and updates the reference.
   */
  create: function create() {
    var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    if (this.storeInstance) return this.storeInstance;
    this.storeInstance = (0, _redux.createStore)(this.getRootReducer(initialState), _redux.compose.apply(void 0, (0, _toConsumableArray2["default"])(this.middlewares)));
    this.sagas.forEach(function (saga) {
      return sagaMiddleware.run(saga);
    });
    return this.storeInstance;
  },

  /**
   * Combines all registered reducers and returns a single reducer function.
   * @param {Object} initialState The initial state for the app
   */
  getRootReducer: function getRootReducer() {
    var _this = this;

    var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    var reducers = _objectSpread({}, this.reducers);

    if (Object.keys(reducers).length === 0 || process.env.NODE_ENV === 'test') {
      reducers.$_foo = function () {
        var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        return state;
      }; // default reducer

    }

    var rootReducer = (0, _redux.combineReducers)(reducers);
    return function () {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
      var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      // start updating the state
      _this.$updatingState = true; // clear getState calls queue

      _this.getStateCallbacks = []; // get the new state object

      var newState = rootReducer(state, action); // invoke each getState call in the queue with the new state

      _this.$updatingState = false;

      while (_this.getStateCallbacks.length) {
        _this.getStateCallbacks.shift()(newState);
      } // return the new state


      return newState;
    };
  },

  /**
   * Returns the complete state object or part of it based on a given query. If the
   * query parameter is a string that uses dot notation, it will return the resolved
   * value of the given key. If the query is an object, it will return an object that
   * has the same structure but contains the resolved values. If the query parameter
   * is not provided, the complete state object will be returned.
   * @param   {String|Object}   query   A query string or a query object that represents
   *                                    part of the state object that needs to be fetched.
   *                                    This parameter is not required.
   * @return  {Promise}                 A promise that eventually resolves with the state
   *                                    object, part of it or a value in the state object.
   */
  getState: function getState(query) {
    var _this2 = this;

    if (this.$updatingState === false) {
      return Promise.resolve(this.getStateSync(query));
    }

    return new Promise(function (resolve) {
      _this2.getStateCallbacks.push(function (state) {
        resolve(_this2.queryState(query, state));
      });
    });
  },
  getStateSync: function getStateSync(query) {
    return this.queryState(query, this.storeInstance.getState());
  },

  /**
   * Queries a state object for a specific value.
   * @param   {String}    query   Query string.
   * @param   {Object}    state   State object to query.
   * @return  {Object}            The state object, part of it or a value in the state object.
   */
  queryState: function queryState(query, state) {
    // handle query strings
    if (helpers.getObjectType(query) === 'string') {
      return helpers.findPropInObject(state, query);
    } // handle query objects


    if (helpers.getObjectType(query) === 'object') {
      return Object.keys(query).reduce(function (prev, next) {
        return _objectSpread({}, prev, (0, _defineProperty2["default"])({}, next, helpers.findPropInObject(state, query[next])));
      }, {});
    }

    return state;
  },

  /**
   * Returns an reference to the Redux store instance.
   */
  getInstance: function getInstance() {
    return this.storeInstance;
  }
};
/**
 * Adds a reducer function to be used by the root reducer.
 * @param  {String}   key       Reducer unique identifier key
 * @param  {Function} reducer   Reducer function.
 */

var useReducer = function useReducer(name, reducer) {
  store.reducers[name] = reducer;
};
/**
 * Allows registering middleware functions such as Router and other middlewares.
 * @param {Function} middleWare Middleware function to use
 */


exports.useReducer = useReducer;

var useMiddleware = function useMiddleware(middleware) {
  store.middlewares.unshift((0, _redux.applyMiddleware)(middleware));
};
/* =================================== */

/* PROVIDER
/* =================================== */


exports.useMiddleware = useMiddleware;

var Provider = function Provider(props) {
  return _react["default"].createElement(_Provider["default"], (0, _extends2["default"])({
    store: store.create(combinedInitialState)
  }, props));
};
/* =================================== */

/* DISPATCH
/* =================================== */

/**
 * Dispatches an action. It may accepts two or three parameters:
 * dispatch(actionType, payload);
 * dispatch(actionObject);
 * @param   {String}  actionType    Type of the action to be dispatched
 * @param   {Object}  payload       Action payload object
 * @param   {Object}  actionObject  Normal action object that contains a 'type' property
 */


exports.Provider = Provider;

function dispatch() {
  var action = {};

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  if (helpers.getObjectType(args[0]) === 'object') {
    action = helpers.deepCopy(args[0]);
  } else if (helpers.getObjectType(args[0]) === 'string') {
    // set the type
    if (/^([^.]*?)\.([^.]*?)$/.test(args[0])) {
      var _args$0$split = args[0].split('.'),
          _args$0$split2 = (0, _slicedToArray2["default"])(_args$0$split, 2),
          moduleName = _args$0$split2[0],
          moduleAction = _args$0$split2[1];

      var camelCaseName = helpers.toCamelCase(moduleAction);
      var actionName = helpers.toSnakeCase(camelCaseName).toUpperCase();
      action.type = "@@".concat(moduleName, "/").concat(actionName);
    } else {
      action.type = args[0];
    } // set the payload


    if (helpers.getObjectType(args[1]) === 'object') {
      action.payload = _objectSpread({}, args[1]);
      args.splice(1, 1);
    } else {
      action.payload = {};
    }
  }

  store.storeInstance.dispatch(action);
}
/* =================================== */

/* MODULE
/* =================================== */


var Module = function Module(config) {
  var _this3 = this;

  (0, _classCallCheck2["default"])(this, Module);
  (0, _defineProperty2["default"])(this, "build", function () {
    /* build action creators ---------- */
    Object.entries(_this3.config.actions || {}).forEach(function (_ref) {
      var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
          name = _ref2[0],
          callback = _ref2[1];

      var camelCaseName = helpers.toCamelCase(name);
      var actionName = helpers.toSnakeCase(camelCaseName).toUpperCase();
      var actionType = "@@".concat(_this3.name, "/").concat(actionName);
      var argNames = helpers.getArgNames(callback);

      _this3.actionCreators[camelCaseName] = function () {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        // build the payload object
        var payload = argNames.reduce(function (prev, next, index) {
          return _objectSpread({}, prev, (0, _defineProperty2["default"])({}, next, args[index]));
        }, {}); // then use it to build the action object

        var actionObject = {
          type: actionType,
          payload: payload
        };
        return actionObject;
      };

      _this3.actionToReducerMap[actionType] = _this3.createSubReducer(actionType, callback, argNames, 'create');
      _this3.sagas[actionType] = _this3.createSaga(actionType);
    });
    /* build handlers ----------------- */

    Object.entries(_this3.config.handlers || {}).forEach(function (_ref3) {
      var _ref4 = (0, _slicedToArray2["default"])(_ref3, 2),
          name = _ref4[0],
          callback = _ref4[1];

      var actionType = name;

      if (/^(.*?)\.(.*?)$/.test(actionType)) {
        var _actionType$split = actionType.split('.'),
            _actionType$split2 = (0, _slicedToArray2["default"])(_actionType$split, 2),
            moduleName = _actionType$split2[0],
            camelCaseName = _actionType$split2[1];

        var actionName = helpers.toSnakeCase(camelCaseName).toUpperCase();
        actionType = "@@".concat(moduleName, "/").concat(actionName);
      }

      var argNames = helpers.getArgNames(callback);
      _this3.actionToReducerMap[actionType] = _this3.createSubReducer(actionType, callback, argNames, 'handle');
      _this3.sagas[actionType] = _this3.createSaga(actionType);
    });
    /* build reducer ------------------ */

    _this3.reducer = function () {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var action = arguments.length > 1 ? arguments[1] : undefined;
      // the action type might be in normal form, such as: '@@prefix/ACTION_NAME'
      // or it may contain a sub action type: '@@prefix/ACTION_NAME/SUB_ACTION_NAME'
      var actionType = action.type;
      var mainActionType = (actionType.match(/@@(.*?)\/((.*?)(?=\/)|(.*?)$)/) || [])[0] || actionType;
      var subActionType = actionType.replace(mainActionType, '').slice(1);
      var actionName = mainActionType.replace(/^@@(.*?)\//, '');
      var newState = state;
      _this3.cachedState = (0, _defineProperty2["default"])({}, _this3.name, state); // if the sub action is 'update', just update the state with the payload object

      if ( // for self actions
      mainActionType === "@@".concat(_this3.name, "/").concat(actionName) && subActionType === 'UPDATE' || // for handled actions
      _this3.actionToReducerMap[mainActionType]) {
        newState = helpers.mergeObjects(state, action.payload || {});
      } // if it's a main action, look for a sub reducer that can handle this action


      _this3.getActionTypeMatchers(actionType).forEach(function (matcher) {
        if (_this3.actionToReducerMap[matcher]) {
          newState = _this3.actionToReducerMap[matcher](newState, action);
        }
      });

      return newState;
    };
    /* map state to props ------------- */


    _this3.mapStateToProps = function (state) {
      var _ref5;

      var _this3$name = _this3.name,
          ownState = state[_this3$name],
          globalState = (0, _objectWithoutProperties2["default"])(state, [_this3$name].map(_toPropertyKey));
      return _ref5 = {}, (0, _defineProperty2["default"])(_ref5, _this3.stateKey, ownState), (0, _defineProperty2["default"])(_ref5, _this3.globalStateKey, globalState), _ref5;
    };
    /* map dispatch to props ---------- */


    _this3.mapDispatchToProps = function (dispatchFunc) {
      return (0, _redux.bindActionCreators)(_this3.actionCreators, dispatchFunc);
    };
    /* combine props ------------------ */


    _this3.combineProps = function (stateProps, dispatchProps, ownProps) {
      var _objectSpread4;

      return _objectSpread({}, ownProps, {}, stateProps, (_objectSpread4 = {}, (0, _defineProperty2["default"])(_objectSpread4, _this3.actionsKey, _objectSpread({}, dispatchProps)), (0, _defineProperty2["default"])(_objectSpread4, _this3.dispatchKey, dispatch), _objectSpread4));
    };
  });
  (0, _defineProperty2["default"])(this, "createSubReducer", function (actionType, callback, argNames, mode) {
    return function () {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      var matchers = _this3.getActionTypeMatchers(action.type);

      if (matchers.includes(actionType)) {
        var callbackResult = _this3.executeCallback(callback, action, argNames, mode);

        var callbackResultType = helpers.getObjectType(callbackResult);
        var stateFragment = callbackResultType === 'object' ? callbackResult : {}; // the saga handler will be called right after the reducer so instead of the saga
        // handler executing the callback again, pass it the cached result

        _this3.cachedCallbackResult = _this3.cachedCallbackResult || {};
        _this3.cachedCallbackResult[action.type] = callbackResult;
        return helpers.mergeObjects(state, stateFragment);
      }

      return state;
    };
  });
  (0, _defineProperty2["default"])(this, "createSaga", function (actionType) {
    return (
      /*#__PURE__*/
      _regenerator["default"].mark(function saga() {
        return _regenerator["default"].wrap(function saga$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                this.workerSagas[actionType] =
                /*#__PURE__*/
                _regenerator["default"].mark(function workerSaga(action) {
                  var _this4 = this;

                  var result, data, isDone, breakAfter, _loop;

                  return _regenerator["default"].wrap(function workerSaga$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          result = this.cachedCallbackResult && this.cachedCallbackResult[actionType]; // check if the callback return value is an iterable (usually a generator function)
                          // if it is an iterable then consume it

                          if (!(result && typeof result[Symbol.iterator] === 'function')) {
                            _context2.next = 19;
                            break;
                          }

                          _context2.prev = 2;
                          // `data` will be assigned to each `next()` call
                          // `isDone` will be true when `next()` returns done as true
                          isDone = false; // the while loop will break after a maximum of 50 calls

                          breakAfter = 50;
                          _loop =
                          /*#__PURE__*/
                          _regenerator["default"].mark(function _loop() {
                            var next, nextResult;
                            return _regenerator["default"].wrap(function _loop$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    _context.next = 2;
                                    return (0, _effects.call)(function () {
                                      return store.getState();
                                    });

                                  case 2:
                                    _this4.cachedState = _context.sent;
                                    next = result.next(data);
                                    nextResult = next.value;
                                    isDone = next.done; // if the yielded value is a Promise, resolve it then continue

                                    if (!(nextResult instanceof Promise)) {
                                      _context.next = 12;
                                      break;
                                    }

                                    _context.next = 9;
                                    return (0, _effects.call)(function () {
                                      return nextResult;
                                    });

                                  case 9:
                                    data = _context.sent;
                                    _context.next = 15;
                                    break;

                                  case 12:
                                    if (!(helpers.getObjectType(nextResult) === 'object')) {
                                      _context.next = 15;
                                      break;
                                    }

                                    _context.next = 15;
                                    return (0, _effects.put)({
                                      type: "".concat(action.type, "/UPDATE"),
                                      payload: nextResult
                                    });

                                  case 15:
                                    breakAfter -= 1; // safety break

                                    if (!(breakAfter === 0)) {
                                      _context.next = 18;
                                      break;
                                    }

                                    throw new Error('An async action handler yielded more than 50 values.');

                                  case 18:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _loop);
                          });

                        case 6:
                          if (isDone) {
                            _context2.next = 10;
                            break;
                          }

                          return _context2.delegateYield(_loop(), "t0", 8);

                        case 8:
                          _context2.next = 6;
                          break;

                        case 10:
                          _context2.next = 12;
                          return (0, _effects.put)({
                            type: "".concat(action.type, "/COMPLETE")
                          });

                        case 12:
                          _context2.next = 19;
                          break;

                        case 14:
                          _context2.prev = 14;
                          _context2.t1 = _context2["catch"](2);
                          window.console.error(_context2.t1);
                          _context2.next = 19;
                          return (0, _effects.put)({
                            type: "".concat(action.type, "/ERROR"),
                            message: _context2.t1.message
                          });

                        case 19:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, workerSaga, this, [[2, 14]]);
                }).bind(this);
                _context3.next = 3;
                return (0, _effects.takeEvery)(actionType, this.workerSagas[actionType]);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, saga, this);
      }).bind(_this3)
    );
  });
  (0, _defineProperty2["default"])(this, "executeCallback", function (callback, action, argNames, mode) {
    var context = _this3.getCallbackContext();

    var callbackArgs = mode === 'create' ? argNames.map(function (arg) {
      return action.payload[arg];
    }) : [action];
    return callback.apply(context, callbackArgs);
  });
  (0, _defineProperty2["default"])(this, "getCallbackContext", function () {
    var self = _this3;
    return _objectSpread({}, self.config.actions, {
      getState: self.getState
    });
  });
  (0, _defineProperty2["default"])(this, "getState", function (query) {
    var state = _this3.cachedState[_this3.name]; // handle query strings

    if (helpers.getObjectType(query) === 'string') {
      return helpers.findPropInObject(state, query);
    } // handle query objects


    if (helpers.getObjectType(query) === 'object') {
      return Object.keys(query).reduce(function (prev, next) {
        return _objectSpread({}, prev, (0, _defineProperty2["default"])({}, next, helpers.findPropInObject(state, query[next])));
      }, {});
    }

    return state;
  });
  (0, _defineProperty2["default"])(this, "getActionTypeMatchers", function (actionType) {
    var regex = /@@(.+?)\/(.+)/;
    var moduleName = '';
    var actionName = actionType;

    if (regex.test(actionType)) {
      var _actionType$match = actionType.match(regex);

      var _actionType$match2 = (0, _slicedToArray2["default"])(_actionType$match, 3);

      moduleName = _actionType$match2[1];
      actionName = _actionType$match2[2];
    }

    return [actionType, // exact action
    "@@".concat(moduleName), // any action by the module
    "@@".concat(moduleName, "/"), // any action by the module (alias)
    "@@".concat(moduleName, "/*"), // any action by the module (alias)
    "@@*/".concat(actionName), // same action dispatched by any module
    "*/".concat(actionName), // same action dispatched by any module (alias)
    '*' // any action
    ];
  });
  this.config = config;
  this.name = config.name;
  this.stateKey = config.stateKey || 'state';
  this.actionsKey = config.actionsKey || 'actions';
  this.dispatchKey = config.dispatchKey || 'dispatch';
  this.globalStateKey = config.globalStateKey || 'globalState';
  this.actionCreators = {};
  this.actionToReducerMap = {};
  this.sagas = {};
  this.workerSagas = {};

  this.reducer = function (state) {
    return state;
  };

  this.build();
};
/* =================================== */

/* CONNECT
/* =================================== */

/**
 * Connects a component to the Redux store and injects its module state and actions into the
 * component props. If the module name is not provided, the name of the component or function
 * will be used instead. If the component definition is an anonymous function, then the module
 * name must be provided in the configuration object. If the initial state is not provided, an
 * empty object will be assumed to be the initial state.
 * @param {Class|Function}    component   The component to be connected.
 * @param {Object}            config      Configuration object that may contain all or some of
 *                                        the following keys:
 *                                          - name
 *                                              Module namespace, which will be used as a key in
 *                                              the state tree and as a prefix for module actions.
 *                                          - state
 *                                              The initial state object for the module. This
 *                                              object is used to populate the Redux state object
 *                                              with initial values.
 *                                          - actions
 *                                              A hash table of all the actions that can be
 *                                              dispatched from the component to update the state.
 *                                          - handlers
 *                                              A hash table of handler function that listen to
 *                                              actions dispatched by the store. The key represents
 *                                              the action type that needs to be handled and the
 *                                              value represents the handler function.
 *                                          - stateKey
 *                                              The stateKey is a string used to inject the module
 *                                              state into the component props.
 *                                              The default value is 'state'.
 *                                          - globalStateKey
 *                                              The globalStateKey is a string used to inject the
 *                                              global state tree, excluding the module state, into
 *                                              the component props.
 *                                              The default value is 'globalState'.
 *                                          - actionsKey
 *                                              The actionsKey is a string used to inject the module
 *                                              action creator functions into the component props.
 *                                              The default value is 'actions'.
 *                                          - dispatchKey
 *                                              The dispatchKey is a string used to inject the
 *                                              dispatch function into the component props.
 *                                              The default value is 'actions'.
 */


var connect = function connect(component, config) {
  if (!component || !config) {
    throw new Error('The \'connect\' function expects a component definition and a valid configuration object as parameters.');
  }

  if (typeof component !== 'function' && Object.getPrototypeOf(component) !== _react.Component) {
    throw new Error('Expected the first parameter to be a pure function or a valid React component class.');
  }

  if (helpers.getObjectType(config) !== 'object') {
    throw new Error('Module configuration must be a valid object.');
  }

  var moduleConfig = _objectSpread({}, config, {
    name: config.name || helpers.getComponentName(component)
  });

  if (!moduleConfig.name) {
    throw new Error('Property \'name\' is missing from the module configuration. Module name is required.');
  }

  if (typeof connect.moduleNames === 'undefined') {
    connect.moduleNames = {};
  }

  if (connect.moduleNames[moduleConfig.name] === true) {
    throw new Error("Name '".concat(moduleConfig.name, "' has already been used by another module, please use a different name."));
  } else {
    connect.moduleNames[moduleConfig.name] = true;
  }

  var module = new Module(moduleConfig);
  var initialState = moduleConfig.initialState || moduleConfig.state || {};
  combinedInitialState[module.name] = helpers.deepCopy(initialState);
  store.reducers[module.name] = module.reducer;
  var connectedComponent = (0, _connect["default"])(module.mapStateToProps, module.mapDispatchToProps, module.combineProps)(component);
  store.sagas = [].concat((0, _toConsumableArray2["default"])(store.sagas), (0, _toConsumableArray2["default"])(Object.values(module.sagas)));
  return connectedComponent;
};

exports.connect = connect;
var _default = {};
exports["default"] = _default;